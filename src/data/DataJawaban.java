package data;

import soal.Jawaban;
import java.util.ArrayList;
import java.util.Scanner;
import static menu.Menu.clearScreen;
import static menu.Menu.tampilan;
import soal.TestCpns;


public class DataJawaban extends DataManagement{
    
    private static ArrayList<Jawaban> arrayJawaban = new ArrayList<>();
    private static float nilai = 0;
    
    
    DataSoal dataSoal = new DataSoal();
    
    
    @Override
    public void tambahData() {
        addArrayJawaban();
    }
    
    public static void addArrayJawaban() {
        arrayJawaban.add( new Jawaban() );
    }
    
    
    public static ArrayList<Jawaban> getArrayJawaban() {
        return arrayJawaban;
    }
    
    public static Jawaban getArrayIndex(int i) {
        return arrayJawaban.get(i);
    }
    
    @Override
    public void lihatData() {
        Scanner input = new Scanner(System.in);
        
        tampilan('+','=', 50);
        System.out.println("|           HASIL SELEKSI TEST CPNS 2020           |");
        tampilan('+','=', 50);
        System.out.println("No.\tNIK\t\t\tNAMA");
        if ( !DataPendaftar.getArrayPendaftar().isEmpty() ){
            
            for (int i = 0; i < DataPendaftar.getArrayPendaftar().size()   ; i++){
                System.out.println(
                        (i+1) + "\t" + DataPendaftar.getArrayIndex(i).getNik()+
                                "\t\t\t" + DataPendaftar.getArrayIndex(i).getNama() );
                System.out.println(
                        "  \t Nilai : " + DataJawaban.getArrayIndex(i).getNilai() );
            }
            
            boolean loop = true;
            while(loop){
                tampilan('+','=', 50);
                System.out.println("| 1. Publikasi hasil seleksi                       |");
                System.out.println("| 2. Kembali ke menu admin                         |");
                tampilan('+','=', 50);

                System.out.print("Masukan Pilihan : ");
                String opsi = input.nextLine();

                if ( opsi.equals("1") ){
                    loop = false;
                    clearScreen();
                    
                    TestCpns.setBukaSeleksi(true);
                    tampilan('+','=', 50);
                    System.out.println("|            Hasil seleksi telah dibuka            |");
                    tampilan('+','=', 50);
                        
                    

                } else if ( opsi.equals("2") ){
                    loop = false;
                    clearScreen();

                } else {
                    tampilan('+','=', 50);
                    System.err.println("Pilihan Tidak ada");
                    tampilan('+','=', 50);
                }

            }
            
        } else {
            System.out.println("  \tData\t\t\tKosong");
            
        }
    }
    
    public static void updateData(int indeks, ArrayList<String> jawaban){
        
        getArrayIndex(indeks).setJawaban(jawaban);
        
        float nilai = 0;
        for ( int i = 0; i < DataSoal.getArraySoal().size(); i++){
            if ( getArrayIndex(indeks).getJawabanIndex(i).equalsIgnoreCase( DataSoal.getArrayIndex(i).getJawabanBenar() )){
                nilai += DataSoal.getArrayIndex(i).getNilai();
            }
        }
        getArrayIndex(indeks).setNilai(nilai);
        getArrayIndex(indeks).setTelahIkutTes(true);
    }
    
    @Override
    public void hapusData() {
        int indeks = DataPendaftar.getIndeks_hapusData();
        getArrayJawaban().remove(indeks);
        
    }
    
    
}
