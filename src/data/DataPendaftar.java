package data;

import java.util.ArrayList;
import java.util.Scanner;
import static menu.Menu.clearScreen;
import static menu.Menu.tampilan;

public class DataPendaftar extends DataUpdate{
    
    
    
    private static ArrayList<Pendaftar> arrayPendaftar = new ArrayList<>();
    private static int indeks_hapusData;
    Scanner input = new Scanner(System.in);
    
    @Override
    void tambahData() {
    }
    
    public static void tambahData(
            String nama, String nik, String no_kk, 
            String tanggal, String bulan, String tahun, 
            String password, int kesempatan_ubah_data) {    
        
        ArrayList<String> jawaban = new ArrayList<>();
        
        
        arrayPendaftar.add( new Pendaftar(
                nama, nik,  no_kk, 
                tanggal,  bulan,  tahun, 
                password, kesempatan_ubah_data
                )
        
        );
        DataJawaban.addArrayJawaban();
    }

    
    public static ArrayList<Pendaftar> getArrayPendaftar() {
        return arrayPendaftar;
    }
    
    public static Pendaftar getArrayIndex(int i) {
        return arrayPendaftar.get(i);
    }
    

    @Override
    public void lihatData() {
        System.out.println("No.\tNIK\t\t\tNAMA");
        if ( !getArrayPendaftar().isEmpty() ){
            
            for (int i = 0; i < getArrayPendaftar().size()   ; i++){
                System.out.println(
                        (i+1) + "\t" + getArrayIndex(i).getNik()+
                                "\t\t\t" + getArrayIndex(i).getNama() );
            }
            
            boolean loop = true;
            while(loop){
                tampilan('+','=', 50);
                System.out.println("| 1. Lihat Data Detail                             |");
                System.out.println("| 2. Kembali ke menu admin                         |");
                tampilan('+','=', 50);

                System.out.print("Masukan Pilihan : ");
                String opsi = input.nextLine();

                if ( opsi.equals("1") ){
                    loop = false;
                    clearScreen();
                    
                    int indeks = cariData();
                    if( indeks != -1 ){                
                        lihatData(indeks);
                        
                    } else {
                        System.out.println("Maaf data yang anda cari tidak ditemukan\n\n");
                        
                    }
                    

                } else if ( opsi.equals("2") ){
                    loop = false;
                    clearScreen();

                } else {
                    tampilan('+','=', 50);
                    System.err.println("Pilihan Tidak ada");
                    tampilan('+','=', 50);
                }

            }
            
        } else {
            System.out.println("  \tData\t\t\tKosong");
            
        }
    }
    
    public void lihatData(int indeks) {
        System.out.println("\nDetail Data Pendaftar");
        System.out.println("Nama          : " + getArrayIndex(indeks).getNama() );
        System.out.println("NIK           : " + getArrayIndex(indeks).getNik());
        System.out.println("No. KK        : " + getArrayIndex(indeks).getNo_kk() );
        System.out.println("Tanggal lahir : " + 
            getArrayIndex(indeks).getTanggal()  + " - "  + 
            getArrayIndex(indeks).getBulan()    + " - "  + 
            getArrayIndex(indeks).getTahun() );
        System.out.println("\n\n");
    }
    
    public int cariData(){
        int indeks = -1;
        System.out.print("Masukkan NIK pendaftar: ");
        String inputNik = input.nextLine();
        for (int i=0; i < getArrayPendaftar().size();i++){
            if ( getArrayIndex(i).getNik().equals(inputNik) ) {
                indeks = i;
                break;
            }
        }
        return indeks;
        
    }
    
    @Override
    public void updateData() {
        if ( getArrayPendaftar().isEmpty()){
            tampilan('+','=', 50);
            System.out.println("|              Data Pendaftar Kosong.              |");
            tampilan('+','=', 50);
        } else {
            int indeks = cariData();
            if( indeks != -1 ){                
                System.out.println("Data " + getArrayIndex(indeks).getNama() + " ditemukan");
                updateData(indeks);
            } else {
                System.out.println("Maaf data yang anda cari tidak ditemukan\n\n");
            }
        }
    }
    
    public void updateData(int indeks) {
        String nama, nik, no_kk, 
            tanggal, bulan, tahun;
        
        System.out.print("\nNama Lengkap: ");
        nama = input.nextLine();
        
        System.out.print("\nNIK: ");
        nik = input.nextLine();
        
        System.out.print("\nNo. KK: ");
        no_kk = input.nextLine();
        
        System.out.print("\nTanggal lahir: ");
        tanggal = input.nextLine();
        
        System.out.print("\nBulan lahir: ");
        bulan = input.nextLine();
        
        System.out.print("\nTahun lahir: ");
        tahun = input.nextLine();
        
        getArrayIndex(indeks).setNama(nama);
        getArrayIndex(indeks).setNik(nik);
        getArrayIndex(indeks).setNo_kk(no_kk);
        getArrayIndex(indeks).setTanggal(tanggal);
        getArrayIndex(indeks).setBulan(bulan);
        getArrayIndex(indeks).setTahun(tahun);
        getArrayIndex(indeks).setKesempatan_ubah_data(0);
        
        System.out.print("Perubahan data sukses\n\n\n");
    }

    @Override
    public void hapusData() {
        if ( getArrayPendaftar().isEmpty()){
            tampilan('+','=', 50);
            System.out.println("|              Data Pendaftar Kosong.              |");
            tampilan('+','=', 50);
        } else {
            int indeks = cariData();
            if( indeks != -1 ){                
                System.out.println("Data " + getArrayIndex(indeks).getNama() + " berhasil dihapus");
                getArrayPendaftar().remove(indeks);
                setIndeks_hapusData(indeks);
                DataJawaban dataJawaban = new DataJawaban();
                dataJawaban.hapusData();
            } else {
                System.out.println("Maaf data yang anda cari tidak ditemukan\n\n");
            }
        }
        
    }

    public static int getIndeks_hapusData() {
        return indeks_hapusData;
    }

    public static void setIndeks_hapusData(int indeks_hapusData) {
        DataPendaftar.indeks_hapusData = indeks_hapusData;
    }
    
    
    
}
