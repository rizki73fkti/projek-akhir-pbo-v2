package menu;
import java.util.Scanner;


public class Menu{
    
    public static void tampilan (char border, char tengah, int jumlah){
        System.out.print(border);
        for ( int i = 0; i < jumlah; i++){
            System.out.print(tengah);
        }
        System.out.println(border);
    }
    
    public static void clearScreen (){
        for ( int i = 0; i < 50; i++){
            System.out.println("\r\n");
        }
    }
    
    
    public void tampilkanMenu(){
        Scanner input = new Scanner(System.in);
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Admin                                         |");
            System.out.println("| 2. Pendaftar                                     |");
            System.out.println("| 3. EXIT ( Fitur Sementara )                      |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
            
            if ( opsi.equals("1") ){
                loop = false;
                clearScreen();
                MenuAdmin admin = new MenuAdmin();
                    
            } else if ( opsi.equals("2") ){
                loop = false;
                clearScreen();
                MenuPendaftar pendaftar= new MenuPendaftar();
                    
            } else if ( opsi.equals("3") ){
                loop = false;
                tampilan('+','=', 50);
                System.out.println("|| ---------------- Terima Kasih ---------------- ||");
                tampilan('+','=', 50);
                    
            } else {
                clearScreen();
                tampilan('+','=', 50);
                System.out.println("Pilihan Tidak ada");
                tampilan('+','=', 50);
            }
            
        }
    }
    
    
}