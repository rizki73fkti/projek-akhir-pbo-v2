package menu;

import data.DataJawaban;
import data.DataPendaftar;
import data.DataSoal;


import java.util.Scanner;

public class MenuAdmin extends Menu{
    
    private int kesempatan_login = projekakhir.ProjekAkhir.getKesempatan_login();
    Menu menuAwal;
    DataPendaftar dataAkun = new DataPendaftar();
    DataSoal dataSoal = new DataSoal();
    DataJawaban dataJawaban = new DataJawaban();
    
    Scanner input = new Scanner(System.in);
    
    public MenuAdmin() {
        if ( kesempatan_login < 1 ) {
            System.out.println("Anda tidak dapat login sebagai admin.\n");
            menuAwal = new Menu();
            menuAwal.tampilkanMenu();
            
        } else {
            login();
        }
    }
    
    
    
    void login(){
        boolean loginLoop = true;
        while ( loginLoop ){	
            
            System.out.println("Kesempatan login: " + kesempatan_login );
            System.out.print("Username: ");
            String id = input.nextLine();
            
            System.out.print("Password: ");
            String pass = input.nextLine();

            if( id.equals("admin") && pass.equals("admin") ){
                    projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                    loginLoop = false;
                    this.tampilkanMenu();
                    
            }else{
                kesempatan_login -= 1;
                System.out.println("\nUsername / Password anda salah");
            }

            if( kesempatan_login == 0 ){
                loginLoop = false;
                projekakhir.ProjekAkhir.setKesempatan_login(kesempatan_login);
                System.out.println("\nAnda tidak dapat login sebagai admin.\n");
                menuAwal = new Menu();
                menuAwal.tampilkanMenu();
            }
	}
    }
    
    @Override
    public void tampilkanMenu(){
        boolean loop = true;
        while(loop){
            tampilan('+','=', 50);
            System.out.println("| 1. Tambah Soal Tes                               |");
            System.out.println("| 2. Lihat Soal Tes                                |");
            System.out.println("| 3. Ubah Soal Tes                                 |");
            System.out.println("| 4. Hapus Soal Tes                                |");
            System.out.println("| 5. Lihat Data Akun                               |");
            System.out.println("| 6. Ubah Data Akun                                |");
            System.out.println("| 7. Hapus Akun                                    |");
            tampilan('+','-', 50);
            System.out.println("| 8. Hasil Seleksi                                 |");
            tampilan('+','-',50);
            System.out.println("| 0. Kembali ke menu awal                          |");
            tampilan('+','=', 50);
            
            System.out.print("Masukan Pilihan : ");
            String opsi = input.nextLine();
    
            switch (opsi) {
                case "1":
                    loop = false;
                    clearScreen();
                    dataSoal.tambahData();
                    tampilkanMenu();
                    break;
                    
                case "2":
                    loop = false;
                    clearScreen();
                    dataSoal.lihatData();
                    tampilkanMenu();
                    break;
                    
                case "3":
                    loop = false;
                    clearScreen();
                    dataSoal.updateData();
                    tampilkanMenu();
                    break;
                    
                case "4":
                    loop = false;
                    clearScreen();
                    dataSoal.hapusData();
                    tampilkanMenu();
                    break;
                
                case "5":
                    loop = false;
                    clearScreen();
                    dataAkun.lihatData();
                    tampilkanMenu();
                    break;
                    
                case "6":
                    loop = false;
                    clearScreen();
                     dataAkun.updateData();
                    tampilkanMenu();
                    break;
                    
                case "7":
                    loop = false;
                    clearScreen();
                     dataAkun.hapusData();
                    tampilkanMenu();
                    break;
                    
                case "8":
                    loop = false;
                    clearScreen();
                    dataJawaban.lihatData();
                    tampilkanMenu();
                    break;
                    
                case "0":
                    loop = false;
                    clearScreen();
                    menuAwal = new Menu();
                    menuAwal.tampilkanMenu();
                    break;
                    
                default:
                    System.err.println("Pilihan Tidak ada");
                    break;
            }
            
        }
    }
    
}
