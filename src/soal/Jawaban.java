package soal;

import java.util.ArrayList;

public class Jawaban {
    private ArrayList<String> jawaban = new ArrayList<>();
    private float nilai = 0;
    private boolean telahIkutTes = false;

    public void setJawaban(ArrayList<String> jawaban) {
        this.jawaban = jawaban;
    }
    
    public ArrayList<String> getJawaban() {
        return jawaban;
    }
    
    public String getJawabanIndex(int i) {
        return jawaban.get(i);
    }

    public void setTelahIkutTes(boolean telahIkutTes) {
        this.telahIkutTes = telahIkutTes;
    }

    public boolean isTelahIkutTes() {
        return telahIkutTes;
    }
    
    
    
    
    public void setNilai(float nilai) {
        this.nilai = nilai;
    }
    
    public float getNilai() {
        return nilai;
    }
    
    
}
