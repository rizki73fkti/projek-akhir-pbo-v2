package soal;

public class Soal {
    private String question, jawaban_1, jawaban_2, jawaban_3, jawaban_4, jawaban_5, jawabanBenar;
    private float nilai;
    
    public Soal(
            String question, String jawaban_1, String jawaban_2, 
            String jawaban_3, String jawaban_4, String jawaban_5, 
            String jawabanBenar ) {
        
        this.question = question;
        this.jawaban_1 = jawaban_1;
        this.jawaban_2 = jawaban_2;
        this.jawaban_3 = jawaban_3;
        this.jawaban_4 = jawaban_4;
        this.jawaban_5 = jawaban_5;
        this.jawabanBenar = jawabanBenar;
        
        
    }
    
    //SETTER
    public void setQuestion(String question) {
        this.question = question;
    }

    public void setJawaban_1(String jawaban_1) {
        this.jawaban_1 = jawaban_1;
    }

    public void setJawaban_2(String jawaban_2) {
        this.jawaban_2 = jawaban_2;
    }

    public void setJawaban_3(String jawaban_3) {
        this.jawaban_3 = jawaban_3;
    }

    public void setJawaban_4(String jawaban_4) {
        this.jawaban_4 = jawaban_4;
    }

    public void setJawaban_5(String jawaban_5) {
        this.jawaban_5 = jawaban_5;
    }
    
    public void setJawabanBenar (String jawabanBenar) {
        this.jawabanBenar = jawabanBenar;
    }
    
    public void setNilai(float nilai) {
        this.nilai = nilai;
    }
    
    
    //GETTER
    public String getQuestion() {
        return question;
    }

    public String getJawaban_1() {
        return jawaban_1;
    }

    public String getJawaban_2() {
        return jawaban_2;
    }

    public String getJawaban_3() {
        return jawaban_3;
    }

    public String getJawaban_4() {
        return jawaban_4;
    }

    public String getJawaban_5() {
        return jawaban_5;
    }

    public String getJawabanBenar() {
        return jawabanBenar;
    }

    public float getNilai() {
        return nilai;
    }
    
}
